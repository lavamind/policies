import collections
import datetime
import re
import urllib
import sys

try:
  from dateutil import relativedelta
except ImportError:
  print("dateutil unavailable, please run 'sudo pip install python-dateutil'")
  sys.exit(1)

EMAIL_LISTS = (
  'tor-talk',
  'tor-dev',
  'tor-relays',
  'tor-project',

  'ooni-talk',
  'ooni-dev',

  'global-south',
  'metrics-team',
  'tbb-dev',
  'tor-community-team',
  'UX',
)

AUTHOR = re.compile('</A><A NAME="\S+">&nbsp;</A>\n<I>(.+)\n')

all_emails = []
emails_for_list = {}  # {author => {list => count}}

for email_list in EMAIL_LISTS:
  all_list_authors = []

  for month_offset in range(6):
    d = datetime.date.today() - relativedelta.relativedelta(months = month_offset)
    url = "https://lists.torproject.org/pipermail/%s/%s/author.html" % (email_list, d.strftime("%Y-%B"))

    request = urllib.urlopen(url)
    list_authors = AUTHOR.findall(request.read())

    all_emails += list_authors
    all_list_authors += list_authors

  for author, count in collections.Counter(all_list_authors).items():
    emails_for_list.setdefault(author, {})[email_list] = count

for author, count in sorted(collections.Counter(all_emails).items(), key = lambda entry: entry[1], reverse = True):
  list_counts = ['%s %s' % (c, a) for (a, c) in sorted(emails_for_list.get(author, {}).items(), key = lambda e: e[1], reverse = True)]

  print('%s %s' % (count, author))
  print('  %s' % ', '.join(list_counts))
  print('')

