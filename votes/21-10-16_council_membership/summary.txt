=====================================================================
Summary
=====================================================================

Topic: Community Council Membership
Date taken: Oct 2 2021 - Oct 16 2021
Vote count: 23 (of 105, 22%)

Vote Secretary: Sebastian Hahn

  Question 1: List up to five of these individuals you'd like to see
  on the new Community Council.

  * Antonela Debiasi (antonela)
  * Cecylia Bocovich (cohosh)
  * Kevin Gallagher (kevun)
  * Narrira (nah)
  * Nick Mathewson (nickm)

  Question 2: Would you be uncomfortable having any of these people
  adjudicate your issues?

Results were...

  * The next council will be composed of Antonela, Cecylia, Kevin, Nah,
    and Nick.

=====================================================================
Votes
=====================================================================

Al Smith
Alexander Færøy
Allen Gunn
Antoine Beaupré
Antonela Debiasi
Cecylia Bocovich
David Goulet
Gaba
Georg Koppen
Ian Goldberg
intrigeri
Joydeep Sen Gupta
kat
Mark Smith
meskio
Nick Mathewson
Nicolas Vigier
Rob Jansen
Roger Dingledine
Silvia/Hiro
Taylor Yu
Tom Ritter
Wendy Seltzer

4465;antonela, cohosh, kevun, nah, nickm;
6243;antonela, cohosh, kevun, nah, nickm;
6443;antonela, cohosh, kevun, nah, nickm;<abstain>
11184;antonela, cohosh, kevun, nah, nickm;
12177;antonela, cohosh, nah, nickm;
20600;antonela, cohosh, nah;
21537;antonela, cohosh, kevun, nah, nickm;
21648;antonela, cohosh, nah;kevun, nickm
22181;antonela, cohosh, kevun, nah, nickm;
24104;antonela, cohosh, kevun, nah, nickm;
24445;antonela, cohosh, nah;<abstain>
26979;antonela, cohosh, nickm;
27590;antonela, cohosh, kevun, nah;
32125;antonela, cohosh, kevun, nah;<abstain>
33799;antonela, cohosh, kevun, nah, nickm;<abstain>
41320;antonela, cohosh, kevun, nah, nickm;<abstain>
43043;antonela;
50527;antonela, cohosh, kevun, nah, nickm;<abstain>
51386;antonela, cohosh, kevun, nah, nickm;
54221;antonela, cohosh, kevun, nah, nickm;<abstain>
57740;antonela, cohosh, kevun, nickm;
62670;antonela, cohosh, kevun, nah, nickm;
63524;antonela, cohosh, kevun, nah, nickm;

=====================================================================
Question 1: List up to five of these individuals you'd like to see
on the new Community Council.
=====================================================================

antonela: 23 (100%)
cohosh: 22 (96%)
kevun: 17 (74%)
nah: 20 (87%)
nickm: 17 (74%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

The next council will be composed of Antonela, Cecylia, Kevin, Nah,
and Nick.

=====================================================================
Question 2: Would you be uncomfortable having any of these people
adjudicate your issues?
=====================================================================

This question had seven abstentions.

antonela: 0 (0%)
cohosh: 0 (0%)
kevun: 1 (4% or 6% depending on interpretation)
nah: 0 (0%)
nickm: 1 (4% or 6% depending on interpretation)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

Nobody reached 25% so everyone's eligible to be on the council.

